# wargaming test task

Create console application that takes exactly one parameter (the relative or absolute path to directory).
In that path it makes search of the files. Then it needs to find the equal files in that list.

## how to build

Everything inside appveyor configuration files

## how it works

std::filesystem used to find all the files. The files are equal when their size and sha256 hash are equal
Relative path is the path which starts from dot - '.'. For example, .\hello is relative path.
To make is absolute we should remove the dot and add current dir prefix to relative path.

## status

[![Build status](https://ci.appveyor.com/api/projects/status/fjcuco4dshn9fyks?svg=true)](https://ci.appveyor.com/project/KulykIevgen/wargaming-test-71njq)
