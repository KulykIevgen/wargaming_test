#pragma once

#include <array>
#include <cstdint>
#include <string>
#include <Windows.h>

#include "sha256.h"
using HashArray = uint8_t[SHA256_BLOCK_SIZE];

struct FileData
{
	uint64_t FileSize;
	HashArray Hash;
};
inline bool operator==(const FileData& first, const FileData& second)
{
	if (first.FileSize == second.FileSize)
	{
		return (memcmp(first.Hash, second.Hash, 
			sizeof(HashArray)) == 0) ? true : false;
	}

	return false;
}

class FileAnalyzer
{
public:
	explicit FileAnalyzer(const std::string& FileName);
	~FileAnalyzer() noexcept;
	FileData GetFileData() const;
	static bool IsEqual(const std::string& FirstFile,
		const std::string& SecondFile);
	FileAnalyzer() = delete;
	FileAnalyzer(const FileAnalyzer&) = delete;
	FileAnalyzer& operator=(const FileAnalyzer&) = delete;
	FileAnalyzer(FileAnalyzer&&) = delete;
	FileAnalyzer& operator=(FileAnalyzer&&) = delete;


private:
	HANDLE fileHandle;

	uint64_t getSizeOfFile() const;
	std::array<uint8_t, SHA256_BLOCK_SIZE> getSha256(uint64_t fileSize) const;
};