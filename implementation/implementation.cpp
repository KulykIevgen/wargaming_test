﻿#include <iostream>
#include "Copmarator.h"
#include "FileBrowser.h"
#include "PathConvertor.h"

int main(int argc, char* argv[]) try
{
	if (argc != 2)
	{
		std::cout << "Shoud pass the full or relative path ot directory" << std::endl;
		std::cout << "Example: implementation.exe C:\\hello\\world" << std::endl;
		std::cout << "Example with relative path: implementation.exe .\\world" << std::endl;
		return -1;
	}	

	const auto dirname = std::string(argv[1]);
	FileBrowser fb(PathConvertor::ToFullPath(dirname));
	const auto groups = fb.GetGroups();
	for (const auto& group : groups)
	{
		std::cout << "Group begins" << std::endl;
		for (const auto& element : group)
		{
			std::cout << element << std::endl;
		}
		std::cout << "Group ends" << std::endl;
		std::cout << std::endl;
	}

	return 0;
}
catch (const std::exception& error)
{
	std::cout << error.what() << std::endl;
}