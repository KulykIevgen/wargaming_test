#include "FileBrowser.h"
#include "Copmarator.h"

namespace fs = std::filesystem;

FileBrowser::FileBrowser(const std::string& DirName)
{
	for (auto& element : fs::recursive_directory_iterator(DirName))
	{
		if (element.is_regular_file())
		{
			filePaths.push_back(element.path().string());
		}
	}
}

std::vector<std::list<std::string>> FileBrowser::GetGroups() const
{
	std::vector<std::list<std::string>> result;
	auto listCopy = filePaths;

	while (listCopy.size() > 1)
	{
		std::list<std::string> oneBlock;
		oneBlock.push_back(*listCopy.cbegin());
		for (auto first = listCopy.cbegin(),
			current = std::next(listCopy.cbegin());
			current != listCopy.cend(); ++current)
		{
			try 
			{
				// not clear from the task what to do,
				// if we are not able to open file

				if (FileAnalyzer::IsEqual(*first, *current))
				{
					oneBlock.push_back(*current);
				}
			}
			catch (const std::exception&){ }
		}

		if (oneBlock.size() > 1)
		{
			result.push_back(std::move(oneBlock));
		}

		listCopy.pop_front();
	}

	return result;
}
