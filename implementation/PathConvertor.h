#pragma once

#include <stdlib.h>
#include <string>

class PathConvertor
{
public:
	static std::string ToFullPath(const std::string& path);
	PathConvertor() = delete;
	~PathConvertor() = delete;
	PathConvertor(const PathConvertor&) = delete;
	PathConvertor(PathConvertor&&) = delete;
	PathConvertor& operator=(const PathConvertor&) = delete;
	PathConvertor& operator=(PathConvertor&&) = delete;
};