#pragma once

#include <filesystem>
#include <list>
#include <string>
#include <vector>

class FileBrowser
{
public:
	explicit FileBrowser(const std::string& DirName);
	~FileBrowser() = default;
	FileBrowser() = delete;
	FileBrowser(const FileBrowser&) = default;
	FileBrowser(FileBrowser&&) = default;
	FileBrowser& operator=(const FileBrowser&) = default;
	FileBrowser& operator=(FileBrowser&&) = default;
	std::vector<std::list<std::string>> GetGroups() const;

private:
	std::list<std::string> filePaths;
};