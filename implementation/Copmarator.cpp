#include "Copmarator.h"

#include <stdexcept>

FileAnalyzer::FileAnalyzer(const std::string& FileName)
{
	fileHandle = CreateFileA(FileName.c_str(), GENERIC_READ,
		FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, 0, NULL);

	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		throw std::exception("Unable to open file");
	}
}

FileAnalyzer::~FileAnalyzer() noexcept
{
	CloseHandle(fileHandle);
}

FileData FileAnalyzer::GetFileData() const
{
	FileData result{ 0 };

	result.FileSize = getSizeOfFile();
	auto hashData = getSha256(result.FileSize);
	memcpy(result.Hash, hashData.data(), hashData.size());

	return std::forward<decltype(result)> (result);
}

bool FileAnalyzer::IsEqual(const std::string& FirstFile, const std::string& SecondFile)
{
	FileAnalyzer f1(FirstFile);
	FileAnalyzer f2(SecondFile);

	const auto data1 = f1.GetFileData();
	const auto data2 = f2.GetFileData();

	return (data1 == data2);
}

uint64_t FileAnalyzer::getSizeOfFile() const
{
	LARGE_INTEGER size{ 0 };
	if (!GetFileSizeEx(fileHandle, &size))
	{
		throw std::exception("Unable to get filesize");
	}

	return static_cast<uint64_t> (size.QuadPart);
}

std::array<uint8_t, SHA256_BLOCK_SIZE> FileAnalyzer::getSha256(uint64_t fileSize) const
{
	std::array<uint8_t, SHA256_BLOCK_SIZE> result;
	SHA256_CTX ctx;
	const uint64_t iterations = fileSize / SHA256_BLOCK_SIZE +
		(fileSize % SHA256_BLOCK_SIZE != 0) ? 1 : 0;
	sha256_init(&ctx);

	for (uint64_t i = 0; i < iterations; i++)
	{
		uint8_t tmp[SHA256_BLOCK_SIZE] = { 0 };
		DWORD stub = 0;
		if (!ReadFile(fileHandle, tmp, sizeof(tmp), &stub, nullptr))
		{
			throw std::exception("Unable to read file data");
		}
		sha256_update(&ctx, tmp, stub);
	}

	sha256_final(&ctx, result.data());
	return std::forward<decltype(result)>(result);	
}
