#include <stdexcept>
#include <Windows.h>
#include "PathConvertor.h"

std::string PathConvertor::ToFullPath(const std::string& path)
{
	if (path.length() && path[0] == '.')
	{
		const auto& relative = std::string(std::next(path.cbegin()), path.cend());
		char full[_MAX_PATH] = { 0 };
		if (!GetCurrentDirectoryA(sizeof(full), full))
		{
			throw std::exception("Unable to find current directory");
		}
		return std::forward<std::string> (std::string(full) + relative);
	}

	return path;
}
